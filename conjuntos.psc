Algoritmo conjuntos
	
	x <- 5 // cantidad de elementos del conjunto A
	v <- 5 // cantidad de elementos del conjunto universal
	z <- 5
	t<-x+z
	r<-x+z
	f1 <- 0
	f2 <- 0 
	f3 <- 0
	f4 <- 0
	aux1<-0
	aux2<-0
	Dimension U[v],A[x],B[z],AuB[t],AnB[r]
	//leer_conjunto(U,v,'universal')
	leer_conjunto(A,x,'A')
	leer_conjunto(B,z,'B')
	i <- '' // A  interseccion B 
	U1 <- '' // A uIion B
	U2 <- ''
	D <- '' // A - B
	D1 <- '' // B - A
	DS <- '' // A DIF SIM B
	O1 <- '' // A COMPLEMENTO
	O2 <- '' // B COMPLEMENTO
	
	Para J<-1 Hasta x Hacer
		AuB[J] <- A[J]
	FinPara
	
	Para J<-1+z Hasta r Hacer
		AuB[J] <- B[J-z]
	FinPara
	
	Para k<-1 hasta r hacer
		Para j<-2 hasta r hacer
		si AuB[k] < AuB[j] entonces
			aux1 <-AuB[k]
			AuB[k] <- AuB[j]
			AuB[j] <-aux1
		FinSi
		Fin Para
	FinPara
	
	Para J<-1 hasta r hacer
		Imprimir AuB[J]
	FinPara
	
	//AnB
	Para J<-1 Hasta x Hacer
		Para K<-1 Hasta z Hacer
			Si A[J]==B[K] Entonces
				i <- i+ConvertirATexto(A[J]) 
			FinSi
		FinPara
	FinPara
	
	//AUB
	Para J<-1 Hasta x Hacer
		U1<-U1+ConvertirATexto(A[J])
	FinPara
	PARA J<-1 HASTA x Hacer
		U1<-U1+ConvertirATexto(B[J])
	FinPara
	
	//A-B
	
	J=1
	K=1
	Mientras J<=x Hacer
		MIENTRAS K<=z Hacer
			Si ! A[k]==B[j] Entonces
				D<-D+ConvertirATexto(A[k])
			FinSi
			K<-K+1
		FinMientras
	J<-J+1
	FinMientras

	//B-A
	Mientras J<=x Hacer
		imprimir J
		MIENTRAS K<=z Hacer
			Si ! B[k]==A[j] Entonces
				D<-D+ConvertirATexto(B[k])
			FinSi
			K<-K+1
			imprimir K
		FinMientras
		J<-J+1
	FinMientras	
	
	//Borrar Pantalla
	Escribir 'MENU'
	Escribir '1 PARA MOSTRAR AUB'
	Escribir '2 PARA MOSTRAR AnB'
	Escribir '3 PARA MOSTRAR A\B'
	Escribir '4 PARA MOSTRAR B\A'
	Escribir '5 PARA MOSTRAR A DIREFENCIA SIMETRICA B'
	Escribir '6 PARA MOSTRAR A COMPLEMENTO'
	Escribir '7 PARA MOSTRAR B COMPLEMENTO'
	Escribir '8 PARA MOSTRAR TODO'
	// U1=AUB"
	// I="AnB"
	// D="A\B"
	// D1="B\A"
	// DS="A DIFERENCIA SIMETRICA B"
	// O1="A COMPLEMENTO"
	// O2="B COMPLEMENTO"
	//T <- 'TODO'
	Leer F
	Segun F  Hacer
		1:
			Escribir U1
		2:
			Escribir i
		3:
			Escribir D
		4:
			Escribir D1
		5:
			Escribir DS
		6:
			Escribir O1
		7:
			Escribir O2
		8:
			Escribir T
		De Otro Modo:
			Escribir 'NO ES UNA OPCION VALIDAD'
	FinSegun
FinAlgoritmo

Funcion leer_conjunto (conjunto,indice,nombre)
	resultado <- ''
	Para i<-1 Hasta indice Hacer
		Escribir 'escriba el valor del conjunto ',nombre,' posicion ',i,':'
		Leer conjunto[i]
		resultado <- resultado+' '+ConvertirATexto(conjunto[i])
	FinPara
	Escribir 'CONJUNTO '+nombre+': '+resultado
FinFuncion

